package com.example.batterysample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.WindowManager;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    PowerManager.WakeLock wakeLock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // onResume などで呼ぶ
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        getApplicationContext().registerReceiver(broadcastReceiver, intentFilter);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyApp::MyWakelockTag");
        wakeLock.acquire();


    }
    @Override
    protected void onDestroy() {

        super.onDestroy();

        getApplicationContext().unregisterReceiver(broadcastReceiver);
        wakeLock.release();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    // フィールド
    int batteryLevel;
    float batteryTemperature;

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int rawLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            if (rawLevel != -1 && scale != -1) { // ↑で渡しているデフォルト値の-1でないことを確認
                float batteryLevelFloat = rawLevel / (float) scale;
                batteryLevel = (int) (batteryLevelFloat * 100); // 0-100 の batteryLevel 値を設定
            }

            // バッテリー温度を取得
            // BatteryManager.EXTRA_TEMPERATURE で取得できる値は ℃x10 なので、
            // 例えば 275 という値が取れた場合は 27.5℃ を表す。
            batteryTemperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0) / 10.0f;

            // Are we charging / charged?
            int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                    status == BatteryManager.BATTERY_STATUS_FULL;

// How are we charging?
            int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
            boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;

            TextView text = findViewById(R.id.battery_temp);
            text.setText("temp:"+batteryTemperature);

            TextView text2 = findViewById(R.id.battery_level);
            text2.setText("level:"+batteryLevel);

            TextView text3 = findViewById(R.id.battery_status);
            text3.setText("status:"+status);
        }
    };

}